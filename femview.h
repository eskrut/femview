#ifndef FEMVIEW_H
#define FEMVIEW_H

#include "QVTKWidget.h"
#include "vtkSmartPointer.h"
#include "vtkUnstructuredGrid.h"
#include "vtkPoints.h"
#include "vtkCellArray.h"
#include "vtkCellData.h"
#include "vtkPointData.h"
#include "vtkCamera.h"
#include "vtkActor.h"
#include "vtkScalarBarActor.h"
#include "vtkDataSetMapper.h"
#include "vtkWarpVector.h"
#include "vtkLookupTable.h"
#include "vtkGeometryFilter.h"

class FEMview : public QVTKWidget
{
    Q_OBJECT
public:
    explicit FEMview(QWidget *parent = 0);
private:
    vtkSmartPointer<vtkUnstructuredGrid> grid_;
    vtkSmartPointer<vtkPoints> points_;
    vtkSmartPointer<vtkCellArray> cells_;
    vtkSmartPointer<vtkRenderer> renderer_;
    vtkSmartPointer<vtkActor> actor_;
    vtkSmartPointer<vtkScalarBarActor> bar_;
    vtkSmartPointer<vtkLookupTable> lt_;
    vtkSmartPointer<vtkDataSetMapper> mapper_;
    vtkSmartPointer<vtkWarpVector> warp_;
signals:

public slots:
    void setModel(/* Some pointers with mesh information */);

};

#endif // FEMVIEW_H
