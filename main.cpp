#include <QApplication>
#include "femview.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    FEMview w;

    w.setModel();

    w.show();

    return a.exec();
}
