#include "femview.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"

FEMview::FEMview(QWidget *parent) :
    QVTKWidget(parent)
{
    //Initialize renderer
    GetRenderWindow()->AddRenderer(renderer_ = vtkRenderer::New());
    renderer_->SetBackground(1.0, 1.0, 1.0);
}

void FEMview::setModel()
{
    points_ = vtkPoints::New();
    cells_ = vtkCellArray::New();
    grid_ = vtkUnstructuredGrid::New();

    //Add points data
    const int numPoints = 100; //From fem grid
    points_->SetNumberOfPoints(numPoints);
    for(int ct = 0; ct < numPoints; ++ct) {
        float x = 0;
        float y = 0;
        points_->SetPoint(ct, x, y, 0);
    }

    //Add elements data
    const int numElems = 3; //From fem grid
    std::vector<VTKCellType> cellTypes;
    cellTypes.reserve(numElems);
    for(int ct = 0; ct < numElems; ++ct) {
        vtkIdType indexes[4];
        indexes[0] = 0;
        indexes[1] = 1;
        indexes[2] = 2;
        indexes[3] = 3;
        cells_->InsertNextCell(4, indexes);
        // !!!!!!!!!!!!!!!!!! check type !!!!!!!!!!!!!!!!!!
        cellTypes.push_back(VTK_POLYGON);
    }
    grid_->SetPoints(points_.GetPointer());
    grid_->SetCells(cellTypes.front(), cells_.GetPointer());
}
